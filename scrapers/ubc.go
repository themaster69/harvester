package scrapers

import (
	"http"
)

type UBCScraper struct {
	url string
	store []string
	filters []func(p Package) ([]Package, error)

	algo uint32
}

// Simple setter for the set algo
func (s *UBCScraper) SetAlgorithm (algorithm uint32)  {
	s.algo = algorithm;
}

// Simple getter for the number of filters
func (s *UBCScraper) GetLevels () int {
	return len(s.filters);
}


// Layer1 scraping, scrapes out the first layer of course info, departments.
// It returns a list of packages with new urls to scrape from.
func scrapeL1(p Package) ([]Package, error) {
	pack := []Package{}
	ch := make(chan struct{error, Url, http.Response})

	for _, url := range p.urls {
		go {
			err, resp := http.Get(url)
			ch <- struct {error, url, http.Response} {err, resp}
		}
	}

  for range os.Args[1:]{
		err, url, resp := <-ch
  }

	return pack, err
}

//Url UBC_URL = "https://courses.students.ubc.ca/cs/servlets/SRVCourseSchedule"?sessyr=2017&req=0&output=2&sesscd=W"
