package scrapers

const (
	DEPTH_FIRST_SEARCH = iota
	BREADTH_FIRST_SEARCH = iota
)

type Url string

type Scraper interface {

	// BFS routines
	GetLevels() uint32
	ScrapeLevel(level uint32) error
	ExportResults() error

	// Global routines
	Reset() error
	SetAlgorithm(algo uint32) error
}

type Package struct {
	url Url
	data string
	dataType string
}

